jQuery(document).ready(function() {
    // modules data
    jQuery('.remote_site_list_parent-modules-link').click(function() {
        jQuery('#'+ jQuery(this).attr('remote_site_list_parent-target')).toggle();
    });
    jQuery('.remote_site_list_parent-modules-row').hide();

    // status data
    jQuery('.remote_site_list_parent-status-link').click(function() {
        jQuery('#'+ jQuery(this).attr('remote_site_list_parent-target')).toggle();
    });
    jQuery('.remote_site_list_parent-status-row').hide();
});

