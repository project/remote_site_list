<?php

function remote_site_list_parent_services_resources() {
  $resources = array(
    'site_registration' => array(
      'operations' => array(
        'create' => array(
          'help' => t('Create a new registration record for remote_site_list_parent module'),
          'file' => array(
            'type' => 'inc',
            'module' => 'remote_site_list_parent',
            'name' => 'remote_site_list_parent.resource',
          ),
          'callback' => 'remote_site_list_parent_site_reg__create',
          'args' => array(
            array(
              'name' => 'site_registration',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'Array of data about the remote site',
              'type' => 'array',
            ),
          ),
          'access arguments' => array('access content'), // maybe want something better for this?
        ),
      ),
    ),
  );
  return $resources;
}

