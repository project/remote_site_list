<?php

/*
 * Callback from services api
 * CRUD for site_registration api - Create
 */
function remote_site_list_parent_site_reg__create($site_registration) {
  $key     = (array_key_exists('key',     $site_registration)) ? $site_registration['key']:     null; 
  $siteurl = (array_key_exists('siteurl', $site_registration)) ? $site_registration['siteurl']: null; 
  $core    = (array_key_exists('core',    $site_registration)) ? $site_registration['core']:    null; 
  $version = (array_key_exists('version', $site_registration)) ? $site_registration['version']: null; 
  $modules = (array_key_exists('modules', $site_registration)) ? $site_registration['modules']: null; 
  $status  = (array_key_exists('status',  $site_registration)) ? $site_registration['status']:  null; 

  $api_key = variable_get('remote_site_list_parent_key', False);
  $msg = '';
  if (!$api_key) {
    $msg = 'Sorry, your remote_site_list instance could not be recorded because the local key isn\'t set up.';
  }
  if (!$key or !$siteurl or !$core or !$version) {
    $msg = 'Sorry, your remote_site_list instance could not be recorded because some parameters were missing.';
  }
  if ($key != $api_key) {
    $msg = 'Sorry, your remote_site_list instance could not be recorded because your key is incorrect.';
  }
  if (!empty($msg)) {
    watchdog('remote_site_list_parent', 'Error with incoming connection from remote site: '. $msg .' Data received was: <pre>'. print_r($site_registration, true) .'</pre>', null, WATCHDOG_ERROR);
    return array('status' => 0, 'message' => $msg);
  }

  $row_id = db_insert('remote_site_list_parent_regs')
    ->fields(array(
      'site' => rawurldecode($siteurl),
      'version_core' => $core,
      'version' => $version,
      'modules' => (!empty($modules)) ? serialize($modules) : null,
      'status' => (!empty($status)) ? serialize($status) : null,
      'datetime' => date('Y-m-d H:i:s.u', REQUEST_TIME),
    ))
    ->execute();
  watchdog('remote_site_list_parent', 'Recorded remote site: '. $siteurl, null, WATCHDOG_INFO);

  return array('status' => 1, 'message' => 'Thanks, your instance has been recorded.');
}

